﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsukeTrail : MonoBehaviour {

	// Use this for initialization
	
    [Range(1, 50)]
    public float startWidht = 5;
    [Range(1, 50)]
    public float endWidht = 2;


	
	// Update is called once per frame
	void Update () {
    GetComponent<TrailRenderer>().time = Random.Range(1, 100);
    GetComponent<TrailRenderer>().startWidth = startWidht;
    GetComponent<TrailRenderer>().endWidth = endWidht;



    }
}
